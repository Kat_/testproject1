{
    "id": "3b45f6bd-ce9c-4468-a76a-834f5e1f1379",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "SprHammer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 23,
    "bbox_right": 40,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cd390ae-9303-4ce8-8848-c97bd3fe1940",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b45f6bd-ce9c-4468-a76a-834f5e1f1379",
            "compositeImage": {
                "id": "6826bb74-71cd-44af-bc9e-b961e4d1d7a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cd390ae-9303-4ce8-8848-c97bd3fe1940",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dc916f1-e023-41fa-a464-4f795e7fc114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cd390ae-9303-4ce8-8848-c97bd3fe1940",
                    "LayerId": "033a5b62-75a8-4cb9-abf2-b9e3e8961f40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "033a5b62-75a8-4cb9-abf2-b9e3e8961f40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b45f6bd-ce9c-4468-a76a-834f5e1f1379",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}