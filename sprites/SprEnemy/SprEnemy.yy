{
    "id": "b9937d75-dbf4-4198-bb49-d1226dc8ce5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "SprEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 26,
    "bbox_right": 42,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b34f6ba5-7ca7-4b57-9ba6-3c640db2629a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9937d75-dbf4-4198-bb49-d1226dc8ce5a",
            "compositeImage": {
                "id": "21cb5d21-73a7-4879-9fea-4ce2c0441ed8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b34f6ba5-7ca7-4b57-9ba6-3c640db2629a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb78eb0b-8916-4340-9644-e1e79d6a19e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b34f6ba5-7ca7-4b57-9ba6-3c640db2629a",
                    "LayerId": "8f5eaef6-122c-46b7-b7eb-4848b58bc8cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8f5eaef6-122c-46b7-b7eb-4848b58bc8cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9937d75-dbf4-4198-bb49-d1226dc8ce5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 105,
    "yorig": 44
}