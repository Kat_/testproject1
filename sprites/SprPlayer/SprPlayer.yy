{
    "id": "daadcd45-67ca-4c79-9521-6312f8b9c36d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "SprPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 22,
    "bbox_right": 41,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64e39b9f-c7c4-431f-8d76-1bcf4ea41e50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daadcd45-67ca-4c79-9521-6312f8b9c36d",
            "compositeImage": {
                "id": "7bddd0ce-6141-4566-860c-7b0f527cf533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64e39b9f-c7c4-431f-8d76-1bcf4ea41e50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00ec9796-acc0-4489-9bfd-56388c0b82f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64e39b9f-c7c4-431f-8d76-1bcf4ea41e50",
                    "LayerId": "c1e19f66-9a72-497b-8032-47548d6b8dad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c1e19f66-9a72-497b-8032-47548d6b8dad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "daadcd45-67ca-4c79-9521-6312f8b9c36d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 148,
    "yorig": 31
}